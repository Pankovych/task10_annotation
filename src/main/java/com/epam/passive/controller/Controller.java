package com.epam.passive.controller;

import com.epam.passive.model.*;

public interface Controller {
    void firstTask(int a,int b, int c);
    void secondTask(String message);
    void thirdTask();
    void fourthTask(String message);
}
