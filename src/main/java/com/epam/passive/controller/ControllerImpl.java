package com.epam.passive.controller;

import com.epam.passive.model.*;
import org.apache.logging.log4j.*;

import java.util.List;
import java.util.OptionalDouble;

public class ControllerImpl implements Controller {
    private Model model;
    private static Logger logger1 = LogManager.getLogger(BusinessLogic.class);

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public final void firstTask(int a, int b, int c) {
        model.maxWithLamda(a, b, c);
        model.averageWithLamda(a, b, c);
    }

    @Override
    public final void secondTask(String message) {
        logger1.info(model.patternAsAnonimus(message));
        logger1.info(model.patternAsLamda(message));
    }

    @Override
    public final void thirdTask() {
        List<Integer> list = model.generateWithIterated();
        int sum = list.stream()
                .reduce((a, b) -> a + b).get();
        logger1.info("sum of list is" + sum);
        int max = list.stream()
                .max(Integer::compare).get();
        logger1.info("max of list is" + max);
        int min = list.stream()
                .min(Integer::compare).get();
        logger1.info("max of list is" + min);
        Double average = list.stream()
                .mapToInt(x -> x)
                .average().getAsDouble();
        logger1.info("max of list is" + average);

        int countNumber = (int) list.stream()
                .mapToDouble(x -> x)
                .filter(x -> x > average).count();
    }

    @Override
    public void fourthTask(String message){

    }
}
