package com.epam.passive.model;

@FunctionalInterface
public interface IcommandLamda {
    String command(String message);
}
